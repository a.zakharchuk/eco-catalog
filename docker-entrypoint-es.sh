#!/bin/bash
cp -R /mnt/analysis-icu /usr/share/elasticsearch/plugins
cd /usr/share/elasticsearch/config
echo "xpack.security.enabled: false" >> elasticsearch.yml
exec /usr/local/bin/docker-entrypoint.sh elasticsearch