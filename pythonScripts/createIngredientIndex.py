from elasticsearch import Elasticsearch, helpers
import csv

if __name__ == '__main__':
    INDEX_NAME = 'ingredients'
    ID_FIELD = 'id'
    TYPE_NAME = ''

    es = Elasticsearch('http://localhost:9200/', request_timeout=300)
    settings = {
        "number_of_shards": 3,
        "number_of_replicas": 1
    }

    # id;
    # name;
    # eco_rating;
    # akne_danger;
    # Origin;
    # INCI;
    # Usage;
    # Danger
    mappings = {
        "properties": {
            "id": {"type": "keyword"},
            "eco_rating": {"type": "integer"},
            "akne_danger": {"type": "integer"},
            "origin": {"type": "text"},
            "inci": {"type": "text"},
            "usage": {"type": "text"},
            "danger": {"type": "text"}
        }
    }

    print("creating 'ingredients' index...")
    es.indices.create(index=INDEX_NAME, settings=settings, mappings=mappings)
    print("created 'ingredients' index...")

    print("start ingredients inserting")

    with open('elastic/ingredients_database.csv', 'r') as outfile:
        reader = csv.DictReader(outfile, delimiter=';')
        helpers.bulk(es, reader, index=INDEX_NAME)
    print("finish ingredients inserting")
