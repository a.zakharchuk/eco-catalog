from bs4 import BeautifulSoup
import requests
import numpy as np

def parseAlphabet(divClass):
    allIngredients = []
    alphabetBlock = soup.find('div', {"class": divClass})
    lettersLinks = alphabetBlock.findChildren("a", recursive=False)
    for letterLink in lettersLinks:
        letterAddress = 'https://ecogolik.com/ingredients/' + letterLink.text + '/'
        responseLetter = requests.get(letterAddress)
        letterSoup = BeautifulSoup(responseLetter.content, "html.parser")
        allIngredients += parseBlock(letterSoup, 'ingredients-block__left-part')
        allIngredients += parseBlock(letterSoup, 'ingredients-block__right-part')
    return allIngredients


def parseBlock(soup, divClass):
    ingredientsInBlock = []
    ingredientBlock = soup.find('div', {"class": divClass})
    ingredientLinks = ingredientBlock.findChildren("a", recursive=False)
    for ingredient in ingredientLinks:
        ingredientName = ingredient.text.strip()
        ingredientsInBlock.append(ingredientName)
    return ingredientsInBlock


if __name__ == '__main__':
    address = 'https://ecogolik.com/ingredients/'
    response = requests.get(address)
    soup = BeautifulSoup(response.content, "html.parser")
    allResults = []
    allResults += parseAlphabet('ingredients-block__number-block')
    allResults += parseAlphabet('ingredients-block__alphabet-block')

# result = np.array(allResults)
# np.savetxt("ingrediensList.txt", result, delimiter=';', fmt="%s")
with open("ingrediensList.txt", "w", encoding="utf-8") as f:
    for item in allResults:
        f.write("%s\n" % item)
