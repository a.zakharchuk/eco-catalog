from bs4 import BeautifulSoup
import requests

def requestSiteStat(package):
    params = '\n'.join(package)
    response = requests.post("https://www.cosdna.com/eng/ingredients.php", data={'q': params})
    soup = BeautifulSoup(response.content, "html.parser")
    resultTable = soup.find('tbody', {'class':'chem-list'})

    ings = list()
    for row in resultTable.findAll("tr"):
        if row.find('span', {'class': 'colors'}):
            name = row.find('span', {'class': 'colors'}).text.strip()
        else:
            name = row.find('span', {'class': 'text-muted'}).text.strip()
            secure = 'N/A'
            resultIng = name + ';' + secure
            ings.append(resultIng)
            continue

        if row.find('span', {'class': 'safety'}):
            secure = ''
            for secures in row.findAll('span', {'class': 'safety'}):
                if len(secure) > 0:
                    secure = (int(secure) + int(secures.text.strip())) / 2
                else:
                    secure += secures.text.strip()
        else:
            secure = '0'

        resultIng = name + ';' + str(secure)
        ings.append(resultIng)
    with open("test.txt", "a", encoding="utf-8") as f:
        for item in ings:
            f.write("%s\n" % item)


if __name__ == '__main__':
    file = open("input.txt", "r", encoding="utf-8")
    ingredients = file.read().split('\n')
    i = 0
    packageSize = 100
    package = []
    result = []

    while i < len(ingredients):
        package.append(ingredients[i])
        i += 1
        if (i % packageSize == 0 or i == len(ingredients)):
            requestSiteStat(package)
            package = []