from elasticsearch import Elasticsearch, helpers
import csv

if __name__ == '__main__':
    INDEX_NAME = 'products'
    ID_FIELD = 'id'
    TYPE_NAME = ''

    es = Elasticsearch('http://localhost:9200/', request_timeout=300)
    settings = {
        "number_of_shards": 3,
        "number_of_replicas": 1
    }

    mappings = {
        "properties": {
            "id": {"type": "keyword"},
            "label": {"type": "text"},
            "brand": {"type": "text"},
            "name": {"type": "text"},
            "price": {"type": "float"},
            "rank": {"type": "double"},
            "ingredients": {"type": "text"},
            "combination": {"type": "integer"},
            "dry": {"type": "integer"},
            "normal": {"type": "integer"},
            "oily": {"type": "integer"},
            "sensitive": {"type": "integer"},
            "eco_rating": {"type": "text"},
            "eco_percentage": {"type": "float"},
            "acne_secure": {"type": "text"},
            "acne_percentage": {"type": "float"}
        }
    }

    print("creating 'products' index...")
    es.indices.create(index=INDEX_NAME, settings=settings, mappings=mappings)
    print("created 'products' index...")

    print("start content reading")

    # with open('elastic/5_products_for_ingredient_create_index.csv', 'r') as outfile:
    with open('elastic/products_READY_RATING_fuzzy.txt', 'r') as outfile:
        reader = csv.DictReader(outfile, delimiter=';')
        helpers.bulk(es, reader, index=INDEX_NAME)
