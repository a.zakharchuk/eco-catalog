from bs4 import BeautifulSoup
import requests
import urllib
import numpy as np
from datetime import date
from datetime import datetime
import time
from urllib.parse import quote


def parseAlphabet(soup, divClass):
    allIngredients = []
    alphabetBlock = soup.find('div', {"class": divClass})
    lettersLinks = alphabetBlock.findChildren("a", recursive=False)
    for letterLink in lettersLinks:
        letterAddress = 'https://ecogolik.com/ingredients/' + letterLink.text + '/'
        responseLetter = urllib.request.urlopen(letterAddress)
        letterSoup = BeautifulSoup(responseLetter, "html.parser")
        parseBlock(letterSoup, 'ingredients-block__left-part')
        parseBlock(letterSoup, 'ingredients-block__right-part')
    return allIngredients


def parseBlock(soup, divClass):
    ingredientsInBlock = []
    ingredientBlock = soup.find('div', {"class": divClass})
    ingredientLinks = ingredientBlock.findChildren("a", recursive=False)
    for ingredient in ingredientLinks:
        ingredientInfoAddress = 'https://ecogolik.com' + quote(ingredient['href'])

        ingredientInfo = urllib.request.urlopen(ingredientInfoAddress)
        ingredientSoup = BeautifulSoup(ingredientInfo, "html.parser")
        try:
            parseIngredientPage(ingredientSoup)
        except Exception:
            print("Error on: " + ingredientInfoAddress)


def parseIngredientPage(soup):
    result = []
    header = soup.find("h1", {"class": 'ingredient-block__title'}).text
    result.append(header)
    dataRaw = soup\
        .find("div", {"class": 'ingredient-block__card-inner'})\
        .findAll('span', {'class':'ingredient-block__card-value'})
    mark = parseMark(soup)
    result.append(str(mark))

    for data in dataRaw:
        result.append(data.text.strip())

    imgPath = 'https://ecogolik.com' + soup.find('div', {'class': 'ingredient-block__photo-block'}).find('img')['src']
    result.append(imgPath)
    addDataToFile(result)


def addDataToFile(data):
    with open("output_test_res.txt", "a", encoding="utf-8") as f:
        f.write(";".join(data))
        f.write("\n")


def parseMark(soup):
    marksSvgs = soup.find('div', {'class':'ingredient-block__card-marks'}).findAll('svg')
    mark = 5
    for markSvg in marksSvgs:
        if markSvg['class'][0] == 'white':
            mark = mark - 1
    return mark


if __name__ == '__main__':
    print("Start: " + str(datetime.now()))
    address = 'https://ecogolik.com/ingredients/'
    response = urllib.request.urlopen('https://ecogolik.com/ingredients/')

    soup = BeautifulSoup(response, "html.parser")
    allResults = []
    allResults += parseAlphabet(soup, 'ingredients-block__number-block')
    allResults += parseAlphabet(soup, 'ingredients-block__alphabet-block')
    print("Finish: " + str(datetime.now()))
