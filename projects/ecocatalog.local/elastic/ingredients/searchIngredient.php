<?php $ingName = $_GET["q"];
$param = "{\"query\":
    {\"fuzzy\": {
      \"name\": {
        \"value\": \"$ingName\",
        \"fuzziness\": \"AUTO\"
        }
    }
  }
 }";
$header = array(
    "content-type: application/json; charset=UTF-8"
);
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "http://es01:9200/ingredients/_search");
curl_setopt($curl,CURLOPT_HTTPHEADER, $header);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
$res = curl_exec($curl);
curl_close($curl);
$ings = json_decode($res)->hits->hits;
$result = "<table \"table-layout\"=\"fixed\" class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\"><thead>
    <th>Id</th>
    <th>Name</th>
    <th>Eco Rating</th>
    <th>Akne Danger</th>
    <th>Origin</th>
    <th>INCI</th>
    <th>Usage</th>
    <th>Danger</th>
</thead>";
foreach ($ings as $ing) {
    $result .= '<tr>';
    $data = $ing->_source;
    $result .= ('<td>' . $data->id . '</td>');
    $result .= '<td>' . $data->name . '</td>';
    $result .= '<td>' . $data->eco_rating . '</td>';
    $result .= '<td>' . $data->akne_danger . '</td>';
    $result .= '<td>' . $data->Origin . '</td>';
    $result .= '<td>' . $data->INCI . '</td>';
    $result .= '<td>' . $data->Usage . '</td>';
    $result .= '<td>' . $data->Danger . '</td>';
    $result .= '</tr>';
}
$result .= "</table>";
echo ($result);
