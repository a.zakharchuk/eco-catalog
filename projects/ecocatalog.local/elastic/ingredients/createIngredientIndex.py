from elasticsearch import Elasticsearch, helpers
import csv


if __name__ == '__main__':
    try: 
        INDEX_NAME = 'ingredients'
        ID_FIELD = 'id'
        TYPE_NAME = ''

        es = Elasticsearch('http://es01:9200/', request_timeout=300)
        settings = {
            "number_of_shards": 3,
            "number_of_replicas": 1
        }

        # id;
        # name;
        # eco_rating;
        # akne_danger;
        # Origin;
        # INCI;
        # Usage;
        # Danger
        mappings = {
            "properties": {
                "id": {"type": "keyword"},
                "name": {"type": "text"},
                "eco_rating": {"type": "integer"},
                "akne_danger": {"type": "integer"},
                "origin": {"type": "text"},
                "inci": {"type": "keyword"},
                "usage": {"type": "text"},
                "danger": {"type": "text"}
            }
        }

        es.indices.create(index=INDEX_NAME, settings=settings, mappings=mappings)

        with open('ingredients_database.csv', 'r',encoding='ISO-8859-1') as outfile:
            reader = csv.DictReader(outfile, delimiter=';')
            helpers.bulk(es, reader, index=INDEX_NAME)
        print("Ingredients imported successfully.")
    except Exception as e:
        print(e)
