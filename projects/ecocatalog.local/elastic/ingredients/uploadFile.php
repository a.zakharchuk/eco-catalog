<?php
$target_dir = "/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], 'ingredients_database.csv');
if (headers_sent() === false)
{
    header('Location: ' . 'http://ecocatalog.local/ingredientsUpload/', true, (false === true) ? 301 : 302);
}
header_remove();
header('Location: ' . '/ingredientsUpload', true, (false === true) ? 301 : 302);
exit();
