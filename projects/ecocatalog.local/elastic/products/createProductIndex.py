from elasticsearch import Elasticsearch, helpers
import csv

if __name__ == '__main__':
    try:
        INDEX_NAME = 'products'
        ID_FIELD = 'id'
        TYPE_NAME = ''

        es = Elasticsearch('http://es01:9200/', request_timeout=300)
        settings = {
            "number_of_shards": 3,
            "number_of_replicas": 1
        }

        mappings = {
            "properties": {
                "id": {"type": "keyword"},
                "label": {"type": "text"},
                "brand": {"type": "text"},
                "name": {"type": "text"},
                "price": {"type": "float"},
                "rank": {"type": "double"},
                "ingredients": {"type": "text"},
                "combination": {"type": "integer"},
                "dry": {"type": "integer"},
                "normal": {"type": "integer"},
                "oily": {"type": "integer"},
                "sensitive": {"type": "integer"},
                "eco_rating": {"type": "text","fielddata":"true"},
                "eco_percentage": {"type":"float"},
                "acne_secure": {"type": "text","fielddata":"true"},
                "acne_percentage": {"type":"float"}
            }
        }

        es.indices.create(index=INDEX_NAME, settings=settings, mappings=mappings)
        with open('products_READY_RATING_fuzzy.txt', 'r',encoding='ISO-8859-1') as outfile:
            reader = csv.DictReader(outfile, delimiter=';')
            helpers.bulk(es, reader, index=INDEX_NAME)
        print("Products imported successfully.")
    except Exception as e:
        print(e)
